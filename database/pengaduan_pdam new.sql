-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 25 Jul 2021 pada 02.37
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pengaduan_pdam`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemutusan`
--

CREATE TABLE `pemutusan` (
  `id` int(11) NOT NULL,
  `id_pelanggan` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `no_sambungan` bigint(20) NOT NULL,
  `rek_bulanan` varchar(50) NOT NULL,
  `angsuran` bigint(20) NOT NULL,
  `biaya_pembukaan` bigint(20) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` date NOT NULL,
  `petugas` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pemutusan`
--

INSERT INTO `pemutusan` (`id`, `id_pelanggan`, `alamat`, `no_sambungan`, `rek_bulanan`, `angsuran`, `biaya_pembukaan`, `keterangan`, `tanggal`, `petugas`, `status`) VALUES
(1, 'Indra', 'Surakarta', 12345, '500000', 100000, 200000, 'nunggak', '2021-07-23', 'Bagas', 'Selesai'),
(2, 'Kusuma', 'Jalan Merak', 1234567, '500000', 100000, 200000, 'nunggak 2 bulan', '2021-07-23', 'Bagas', 'Pending');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaduan`
--

CREATE TABLE `pengaduan` (
  `id` int(11) NOT NULL,
  `kode` varchar(50) NOT NULL,
  `id_user` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `nohp` varchar(20) NOT NULL,
  `jenis` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `deskripsi` text NOT NULL,
  `foto` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `petugas` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengaduan`
--

INSERT INTO `pengaduan` (`id`, `kode`, `id_user`, `alamat`, `nohp`, `jenis`, `tanggal`, `deskripsi`, `foto`, `status`, `petugas`) VALUES
(36, 'PGDM-210724-040527', 'Juminah', 'Jalan Ayam', '09090909', 'Air Keruh', '2021-07-24', 'Air keruh', '', 'Selesai', 'Amir'),
(37, 'PGDM-210724-040652', 'Michael', 'Jalan Ganas', '08080808', 'Pemakaian', '2021-07-24', 'Pemakaian kurang', '', 'Pending', 'Amir'),
(38, 'PGDM-210724-041623', '26', '085864518811', '', 'Meter', '2021-07-24', 'kurang meter', '', 'Pending', NULL),
(39, 'PGDM-210724-041730', 'Riska', '085864518811', '', 'Kebocoran', '2021-07-24', 'bocor di bagian pipa', '', 'Pending', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sambung_kembali`
--

CREATE TABLE `sambung_kembali` (
  `id` int(11) NOT NULL,
  `id_pelanggan` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `no_sambungan` bigint(20) NOT NULL,
  `rek_bulanan` varchar(50) NOT NULL,
  `angsuran` bigint(20) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tindak_lanjut`
--

CREATE TABLE `tindak_lanjut` (
  `id` int(11) NOT NULL,
  `id_pelanggan` varchar(100) NOT NULL,
  `id_pengaduan` int(11) NOT NULL,
  `id_pemutusan` int(11) NOT NULL,
  `tgl_dikerjakan` date NOT NULL,
  `tgl_selesai` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tindak_lanjut`
--

INSERT INTO `tindak_lanjut` (`id`, `id_pelanggan`, `id_pengaduan`, `id_pemutusan`, `tgl_dikerjakan`, `tgl_selesai`) VALUES
(13, 'Indra', 0, 1, '2021-07-23', '2021-07-24'),
(14, 'Juminah', 36, 0, '2021-07-24', '2021-07-24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(128) NOT NULL,
  `no_sambungan` int(11) NOT NULL DEFAULT 0,
  `nohp` varchar(15) NOT NULL,
  `address` text NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) DEFAULT 0,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `email`, `no_sambungan`, `nohp`, `address`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(25, 'Sinyo', 'petugas_lapangan', 'ridwan.andriansyah73@gmail.com', 0, '085864518811', 'Jl. Brawijaya Gg III', '', '$2y$10$lDGcPInuw35FmQKiOhf5p.9..X8i9Hpr5.Xge6X.MKuKJZs0c1R4m', 2, 1, 1625487105),
(26, 'Riska', 'pelanggan', 'riska@gmail.com', 112233, '085864518811', 'Jl. Brawijaya Gg III', '', '$2y$10$B1KKW1uUZeag.B6il7ufeOW4p2UphgNgIyWHD6Vs3z1emOMHd5yA.', 3, 1, 1625487238),
(27, 'Reyhan', 'kabag_langganan', 'rey@gmail.com', 0, '085864518811', 'Jl. Brawijaya Gg III', '', '$2y$10$avySSLsJMUkywNuj469Qdero03T.2OaEcADbaHuUiHqYAWsQIlBYW', 4, 1, 1625487139),
(28, 'Syahrul Direktur', 'direktur', 'sahrul@gmail.com', 0, '085864518811', 'Jl. Brawijaya Gg III', '', '$2y$10$3YeqRvc6dUkYSj2/dXU0IOakvp4hneetJBZ9CQfJ/C4oZRd37rTo.', 5, 1, 1625671084),
(30, 'Mahmud', 'pelanggan_baru', 'mahmud@gmail.com', 0, '085864518811', 'Jl. Brawijaya Gg III', '', '$2y$10$FCESpNKM.yqiad8aFBr2KOcneiTGFB9KZGu74WroxeXt5m8nne.LW', 3, 1, 1625487259),
(31, 'Jenny', 'admin', 'jeny@gmail.com', 0, '085864518811', 'Jl. Brawijaya Gg III', 'a93f2cad4e', '$2y$10$M983yJEMJI1l7lEjPQTzZ.QmEj2YQA9H/NKiiFDsCjo4HjosLiO56', 1, 1, 1625991678),
(32, 'Pelanggan Lagi', 'pelanggan2', 'pelanggan@g.com', 123321123, '098989898989', 'jalan alamat', '', '$2y$10$5NEWduX.ksGHs6qqXsRAN.3m82DwuOLA0u6P0Ip0mqwJ/oXfyjybO', 3, 0, 1624852781),
(33, 'Jihad', 'jihad', 'jidah@gmail.com', 0, '09098989', 'Sukabumi', '', '$2y$10$dE/aef0iyYmKg7BKUxULOeHhSwxvObBR5AKgDAMkrJotlUZcLzO8C', 3, NULL, 1625713408),
(34, 'ridwan', 'ridwan', 'ridwan@gmail.com', 0, '08080808', 'sukabumi', '', '$2y$10$31rF9KDt8BDYRl3oUYJ/wen006tsGWdCOqjrtGeF1KYBBXIOoFFD6', 3, 1, 1626062324),
(36, 'Hasan', 'petugas2', 'bagas@gmail.com', 0, '085577889933', 'Jalan Kenari', '', '$2y$10$JbN1vR3lARgR7HGt2i.afua9WTSLKePB4DZ0jI9YnGTvZZfZW4SZy', 2, 1, 1627033673);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 15),
(2, 1, 1),
(3, 1, 2),
(4, 3, 3),
(5, 3, 4),
(6, 2, 5),
(7, 2, 6),
(8, 5, 7),
(9, 5, 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`) VALUES
(1, 'Administrator'),
(2, 'Menu Admin'),
(3, 'Pelanggan'),
(4, 'Menu Pelanggan'),
(5, 'Petugas Lapangan'),
(6, 'Menu Petugas Lapangan'),
(7, 'Direktur'),
(8, 'Menu Direktur');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Petugas Langganan (admin)'),
(2, 'Petugas Lapangan'),
(3, 'Pelanggan'),
(4, 'Kabag Langganan'),
(5, 'Direktur');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Dashboard', 'admin', 'fa fa-tachometer-alt', 1),
(2, 2, 'Kelola User', 'admin/kelola_user', 'fa fa-users', 1),
(3, 2, 'Kelola Pelanggan', 'admin/kelola_pelanggan', 'fa fa-users', 1),
(4, 2, 'Kelola Pengaduan', 'admin/kelola_pengaduan', 'fa fa-book', 1),
(5, 2, 'Monitoring Pengaduan', 'admin/monitoring_pengaduan', 'fa fa-desktop', 1),
(6, 2, 'Kelola Tindak Lanjut', 'admin/kelola_tindak_lanjut', 'fa fa-network-wired', 1),
(7, 3, 'Dashboard', 'pelanggan', 'fa fa-tachometer-alt', 1),
(8, 4, 'Pengaduan', 'pelanggan/pengaduan', 'fa fa-book', 1),
(9, 4, 'Montoring Pengaduan', 'pelanggan/monitoring_pengaduan', 'fa fa-desktop', 1),
(10, 5, 'Dashboard', 'petugas_lapangan', 'fa fa-tachometer-alt', 1),
(11, 6, 'Monitoring Pengaduan', 'petugas_lapangan/monitoring_pengaduan', 'fa fa-desktop', 1),
(12, 6, 'Kelola Tindak Lanjut', 'petugas_lapangan/kelola_tindak_lanjut', 'fa fa-network-wired', 1),
(14, 7, 'Dashboard', 'direktur', 'fa fa-tachometer-alt', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `pemutusan`
--
ALTER TABLE `pemutusan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengaduan`
--
ALTER TABLE `pengaduan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- Indeks untuk tabel `sambung_kembali`
--
ALTER TABLE `sambung_kembali`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tindak_lanjut`
--
ALTER TABLE `tindak_lanjut`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `pemutusan`
--
ALTER TABLE `pemutusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `pengaduan`
--
ALTER TABLE `pengaduan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT untuk tabel `sambung_kembali`
--
ALTER TABLE `sambung_kembali`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tindak_lanjut`
--
ALTER TABLE `tindak_lanjut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
